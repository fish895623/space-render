let g:renderspacestatus = 'false'
function! RenderSpace()
		if g:renderspacestatus == 'false'
				let g:renderspacestatus = 'true'
				set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
				set list
		else
				let g:renderspacestatus = 'false'
				set list!
		endif
endfunction

